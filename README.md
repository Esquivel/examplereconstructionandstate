# ExampleReconstructionAndState

En este ejercicio podremos ver como manejar correctamente la reconstrucción parcial y total de una Activity, incluso al recibir un resultado de otra Activity.

In this exercise we can see how to correctly manage the partial and total reconstruction of an Activity, even when receiving a result from another Activity.

## Métodos involucrados en este ejercicio:

- onCreate()
- onActivityResult()
- onRestoreInstanceState()
- onResume()
- onSaveInstanceState()

