package com.example.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class DetailActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String RESULT_TEXT_KEY = "result_text_key";
    private static final String STATE_KEY = "state_key";

    private EditText editText;

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        editText = findViewById(R.id.edit);
        final Button button = findViewById(R.id.button);
        button.setOnClickListener(this);
    }

    @Override
    protected void onRestoreInstanceState(final Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            editText.setText(savedInstanceState.getString(STATE_KEY));
        }
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_KEY, editText.getText().toString());
    }

    private void completeFlow() {
        final String resultText = editText.getText().toString();
        final Intent intent = new Intent();
        intent.putExtra(RESULT_TEXT_KEY, resultText);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onClick(final View v) {
        completeFlow();
    }
}