package com.example.myapplication;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int DETAIL_ACTIVITY_REQUEST_CODE = 5050;
    private static final String STATE_KEY = "state_key";

    private String resultText;
    private TextView textView;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.text);
        final Button button = findViewById(R.id.button);
        button.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, @Nullable final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (isDetailActivityResult(requestCode) && isOperationSuccessfull(resultCode)) {
            resultText = data.getStringExtra(DetailActivity.RESULT_TEXT_KEY);
        }
    }

    @Override
    protected void onRestoreInstanceState(final Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            if (resultText == null) {
                resultText = savedInstanceState.getString(STATE_KEY);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        textView.setText(resultText);
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_KEY, resultText);
    }

    @Override
    public void onClick(final View v) {
        startDetailActivity();
    }

    private void startDetailActivity() {
        final Intent intent = new Intent(MainActivity.this, DetailActivity.class);
        startActivityForResult(intent, DETAIL_ACTIVITY_REQUEST_CODE);
    }

    private boolean isDetailActivityResult(final int requestCode) {
        return DETAIL_ACTIVITY_REQUEST_CODE == requestCode;
    }

    private boolean isOperationSuccessfull(final int resultCode) {
        return RESULT_OK == resultCode;
    }
}